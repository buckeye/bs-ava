open Ava

type ctx = {
  foo: string;
}

let () =
test "passing test should pass" (fun t -> Test.pass t);

test "a non-empty string should be truthy" (fun t -> Assert.truthy t "foo" );

test "an empty string should be falsy" (fun t -> Assert.falsy t "");

test "true should be true" (fun t -> Assert.true_ t true);

test "false should be false" (fun t -> Assert.false_ t false);

test "is should pass for 2 numbers" (fun t -> Assert.is t 42 42);

test "isnt should pass for 2 objects" (fun t ->
  Assert.isnt t Js.Obj.empty Js.Obj.empty
);

test "deepEqual should pass for 2 equal arrays" (fun t ->
  Assert.deepEqual t [| 1; 2; 3; 4; |] [| 1; 2; 3; 4; |]
);

test "deepEqual should pass for 2 equal lists" (fun t ->
  Assert.deepEqual t [ 1; 2; 3; 4; ] [ 1; 2; 3; 4; ]
);

test "notDeepEqual should pass for 2 differing arrays" (fun t ->
  Assert.notDeepEqual t [| 1; 2; 3; 4; |] [| 1; 3; 3; 4; |]
);

test "deepEqual should pass for 2 differing lists" (fun t ->
  Assert.notDeepEqual t [ 1; 2; 3; 4; ] [ 4; 2; 3; 4; ]
);

test "plan should pass with the correct number of assertions" (fun t ->
  let _ = Test.plan t 2 in
  let _ = Assert.true_ t true in
  Assert.false_ t false
);

Async.test ava "should pass with a callback" (fun t ->
  let _ = Js.Global.setTimeout
    (fun () -> let _ = Test.pass t in Test.finish t)
    10
  in
  ()
);

test "Setting a context" (fun t ->
  let context = { foo = "bar"; } in
  let _ = Test.contextSet t context in
  let { foo } = Test.contextGet t in
  Assert.is t foo "bar"
);
