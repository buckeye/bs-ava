
type after

type afterEach

type before

type beforeEach

type serial

type ava = {
  after: after;
  afterEach: afterEach;
  before: before;
  beforeEach: beforeEach;
  serial: serial;
}

external ava : ava = "ava" [@@bs.val][@@bs.module]

module Test = struct
  type 'a t = {
    title: string;
    mutable context: 'a;
  } [@@bs.deriving abstract]

  module Internal = struct
    external plan : 'a t -> int -> unit = "plan" [@@bs.send]

    external pass : 'a t -> string option -> unit = "pass" [@@bs.send]

    external fail : 'a t -> string option -> unit = "fail" [@@bs.send]

    external finish : 'a t -> string option -> unit = "end" [@@bs.send]
  end

let plan t count = Internal.plan t count

let pass ?message t = Internal.pass t message

let fail ?message t = Internal.fail t message

let finish ?message t = Internal.finish t message

end

module Assert = struct
  type 'a t = 'a Test.t

  module Internal = struct

    external truthy : 'a t -> 'b -> string option -> unit = "truthy"
    [@@bs.send]

    external falsy : 'a t -> 'b -> string option -> unit = "falsy"
    [@@bs.send]

    external true_ : 'a t -> 'b -> string option -> unit = "true"
    [@@bs.send]

    external false_ : 'a t -> 'b -> string option -> unit = "false"
    [@@bs.send]

    external is : 'a t -> 'b -> 'b -> string option -> unit = "is"
    [@@bs.send]

    external isnt : 'a t -> 'b -> 'b -> string option -> unit = "not"
    [@@bs.send]

    external deepEqual : 'a t -> 'b -> 'b -> string option -> unit = "deepEqual"
    [@@bs.send]

    external notDeepEqual : 'a t -> 'b -> 'b -> string option -> unit = "notDeepEqual"
    [@@bs.send]

    external throws :
      'a t -> (unit -> unit) -> 'b -> string option -> unit = "throws"
    [@@bs.send]

    external notThrows : 'a t -> (unit -> unit) -> string option -> unit = "notThrows"
    [@@bs.send]

    external regex : 'a t -> string -> Js.Re.t -> string option -> unit = "regex"
    [@@bs.send]

    external notRegex : 'a t -> string -> Js.Re.t -> string option -> unit = "notRegex"
    [@@bs.send]

    (* @TODO - https://github.com/avajs/ava#snapshotexpected-message *)

    (* @TODO - https://github.com/avajs/ava#snapshotexpected-options-message *)
  end

  let truthy ?message t value = Internal.truthy t value message

  let falsy ?message t value = Internal.falsy t value message

  let true_ ?message t value = Internal.true_ t value message

  let false_ ?message t value = Internal.false_ t value message

  let is ?message t expected actual =
    Internal.is t actual expected message

  let isnt ?message t expected actual =
    Internal.isnt t actual expected message

  let deepEqual ?message t expected actual =
    Internal.deepEqual t actual expected message

  let notDeepEqual ?message t expected actual =
    Internal.notDeepEqual t actual expected message

  let throws ?message ~expected t fn =
    Internal.throws t fn expected message

  let notThrows ?message t fn = Internal.notThrows t fn message

  let regex ?message t regex value = Internal.regex t value regex message

  let notRegex ?message t regex value = Internal.notRegex t value regex message

end

(* @TODO - https://github.com/avajs/ava#test-macros *)

module Internal = struct
  external test : string -> ('a Test.t -> unit) -> unit = "ava" [@@bs.module]

  external testPromise : string -> ('a Test.t -> 'b Js.Promise.t) -> unit
  = "ava"
  [@@bs.module]
end

let test title impl = Internal.test title impl

let testPromise title impl = Internal.testPromise title impl

let call_with_maybe_msg fn fn_with_msg ava user_fn message =
  match message with
  | None -> fn ava user_fn
  | Some message -> fn_with_msg message ava user_fn

module After = struct

  module Internal = struct
    external all_with_msg: string -> ava -> ('a Test.t -> unit) -> unit
    = "after"
    [@@bs.send]

    external all : ava -> ('a Test.t -> unit) -> unit
    = "after"
    [@@bs.send]

    external promise_all_with_msg :
    string -> ava -> ('a Test.t -> 'b Js.Promise.t) -> unit
    = "after"
    [@@bs.send]

    external promise_all : ava -> ('a Test.t -> 'b Js.Promise.t) -> unit
    = "after"
    [@@bs.send]

    external each_with_msg :
    string -> ava -> ('a Test.t -> unit) -> unit
    = "afterEach"
    [@@bs.send]

    external each : ava -> ('a Test.t -> unit) -> unit
    = "afterEach"
    [@@bs.send]

    external promise_each_with_msg :
    string -> ava -> ('a Test.t -> 'b Js.Promise.t) -> unit
    = "afterEach"
    [@@bs.send]

    external promise_each : ava -> ('a Test.t -> 'b Js.Promise.t) -> unit
    = "afterEach"
    [@@bs.send]
  end

  let all t ?message fn =
    call_with_maybe_msg Internal.all Internal.all_with_msg t fn message

  let promiseAll t ?message fn =
    call_with_maybe_msg
      Internal.promise_all Internal.promise_all_with_msg t fn message 

  let each t ?message fn =
    call_with_maybe_msg Internal.each Internal.each_with_msg t fn message

  let promiseEach t ?message fn =
    call_with_maybe_msg
      Internal.promise_each Internal.promise_each_with_msg t fn message
end

module Async = struct
  external test : ava -> string -> ('a Test.t -> unit) -> unit
  = "cb"
  [@@bs.send]
end

module Before = struct
  module Internal = struct
    external all_with_msg: string -> ava -> ('a Test.t -> unit) -> unit
    = "before"
    [@@bs.send]

    external all : ava -> ('a Test.t -> unit) -> unit
    = "before"
    [@@bs.send]

    external promise_all_with_msg :
    string -> ava -> ('a Test.t -> 'b Js.Promise.t) -> unit
    = "before"
    [@@bs.send]

    external promise_all : ava -> ('a Test.t -> 'b Js.Promise.t) -> unit
    = "before"
    [@@bs.send]

    external each_with_msg :
    string -> ava -> ('a Test.t -> unit) -> unit
    = "beforeEach"
    [@@bs.send]

    external each : ava -> ('a Test.t -> unit) -> unit
    = "beforeEach"
    [@@bs.send]

    external promise_each_with_msg :
    string -> ava -> ('a Test.t -> 'b Js.Promise.t) -> unit
    = "beforeEach"
    [@@bs.send]

    external promise_each : ava -> ('a Test.t -> 'b Js.Promise.t) -> unit
    = "beforeEach"
    [@@bs.send]
  end

  let all t ?message fn =
    call_with_maybe_msg Internal.all Internal.all_with_msg t fn message

  let promiseAll t ?message fn =
    call_with_maybe_msg
      Internal.promise_all Internal.promise_all_with_msg t fn message 

  let each t ?message fn =
    call_with_maybe_msg Internal.each Internal.each_with_msg t fn message

  let promiseEach t ?message fn =
    call_with_maybe_msg
      Internal.promise_each Internal.promise_each_with_msg t fn message
end

module Failing = struct
  external test : ava -> string -> ('a Test.t -> unit) -> unit = "failing"
  [@@bs.send]

  external testPromise :
    ava -> string -> ('a Test.t -> 'b Js.Promise.t) -> unit
  = "failing"
  [@@bs.send]
end

module Only = struct
  external test : ava -> string -> ('a Test.t -> unit) -> unit = "only"
  [@@bs.send]

  external testPromise :
    ava -> string -> ('a Test.t -> 'b Js.Promise.t) -> unit
  = "only"
  [@@bs.send]
end

module Serial = struct
  external test : ava -> string -> ('a Test.t -> unit) -> unit = "serial"
  [@@bs.send]

  external testPromise :
    ava -> string -> ('a Test.t -> 'b Js.Promise.t) -> unit
  = "serial"
  [@@bs.send]
end

module Skip = struct
  external test : ava -> string -> ('a Test.t -> unit) -> unit = "skip"
  [@@bs.send]

  external testPromise :
    ava -> string -> ('a Test.t -> 'b Js.Promise.t) -> unit
  = "skip"
  [@@bs.send]
end

module Todo = struct
  external test : ava -> string -> unit = "todo"
  [@@bs.send]
end
